from django.db import models

class Author(models.Model):
    authorId = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=100)
    phone_no = models.CharField(max_length=13, null=True, blank=True)
    birth_date = models.DateField()
    death_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.authorId

class Category(models.Model):
    categoryId = models.CharField(max_length=100, primary_key=True)
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Book(models.Model):
    bookId = models.CharField(max_length=20, primary_key=True)
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name="book_author")
    publisher = models.CharField(max_length=100)
    publish_date = models.DateField()
    categoryId = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="book_category")
    price = models.IntegerField()
    sold_count = models.IntegerField()

    def __str__(self):
        return self.bookId


