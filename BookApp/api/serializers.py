from rest_framework import serializers
from BookApp.models import Book, Author, Category

class CreateBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = "__all__"

class AddCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"

class AuthorSerializer(serializers.ModelSerializer):
    book_author = CreateBookSerializer(many=True, read_only=True)
    book_category = AddCategorySerializer(many=True, read_only=True)

    class Meta:
        model = Author
        fields = "__all__"
        
class AuthorNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('name',)

class GetAllCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        exclude = ('categoryId',)


class MostSoldBookInCategorySerializer(serializers.ModelSerializer):
    author_ID = serializers.CharField(source='author.authorId')
    author_name = serializers.CharField(source='author.name')
    category = serializers.CharField(source='categoryId.name')

    class Meta:
        model = Book
        exclude = ('categoryId', 'author',)

class GetBookByAuthorIDSerializer(serializers.ModelSerializer):
    author_ID = serializers.CharField(source='author.authorId')
    category = serializers.CharField(source='categoryId.name')

    class Meta:
        model = Book
        exclude = ('categoryId', 'author',)

class SearchBookSerializer(serializers.ModelSerializer):
    author_name = serializers.CharField(source='author.name')
    category = serializers.CharField(source='categoryId.name')

    class Meta:
        model = Book
        exclude = ('categoryId', 'author',)