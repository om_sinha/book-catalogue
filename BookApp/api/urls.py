from django.urls import path
from BookApp.api.views import getListOfCategories, getAllAuthorName, addCategory, addAnAuthor, addBookToCatalogue, searchBook, getBooksByAuthorID, getMostBooksSoldByAuthor, getMostBooksSoldByCategory


urlpatterns = [
    path('add_author/', addAnAuthor),
    path('add_book/', addBookToCatalogue),
    path('add_category/', addCategory),
    path('list_of_categories/', getListOfCategories),
    path('list_of_author/', getAllAuthorName),
    path('most_books_sold_by_author/<str:authorID>/', getMostBooksSoldByAuthor),
    path('most_books_sold_by_category/<str:category>/', getMostBooksSoldByCategory),
    path('book/<str:search_keyword>/', searchBook),
    path('book_by_author/<str:authorID>/', getBooksByAuthorID),
]
