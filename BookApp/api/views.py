from rest_framework.decorators import api_view
from BookApp.models import Book, Author, Category
from BookApp.api.serializers import CreateBookSerializer, AuthorSerializer, AddCategorySerializer, AuthorNameSerializer, GetAllCategorySerializer, MostSoldBookInCategorySerializer, GetBookByAuthorIDSerializer, SearchBookSerializer
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q


@api_view(['POST'])
def addAnAuthor(request):                                 # Create a new author 
    try:
        serializer = AuthorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
            return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def addBookToCatalogue(request):                          # Create a new Book
    try:
        serializer = CreateBookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def addCategory(request):                                 # Create a new Category
    try:
        serializer = AddCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def getListOfCategories(request):                        # Get the list of Categories
    try:
        categoriesData = Category.objects.all()
        if len(categoriesData) > 0:
            serializer = GetAllCategorySerializer(categoriesData, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND) 
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def getAllAuthorName(request):                           # Get the list of all the Authors
    try:
        allAuthorData = Author.objects.all()
        if len(allAuthorData)> 0:
            serializer = AuthorNameSerializer(allAuthorData, many=True)
            return Response(serializer.data)
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND) 
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def getMostBooksSoldByAuthor(request, authorID):          # Get the most sold book by an author using the Author ID
    try:
        authorData = Author.objects.filter(authorId=authorID)
        if len(authorData) > 0:
            bookData = Book.objects.filter(author=authorData[0]).order_by('-sold_count').first()
            if bookData:
                serializer = GetBookByAuthorIDSerializer(bookData)
                return Response(serializer.data)
            else:
                return Response([], status=status.HTTP_404_NOT_FOUND)    
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def getMostBooksSoldByCategory(request, category):         # Get the most sold book In the Category using the category name
    try:
        categoryData = Category.objects.filter(name__icontains=category)
        if len(categoryData) > 0:
            bookData = Book.objects.filter(categoryId=categoryData[0]).order_by('-sold_count').first()
            if bookData:
                serializer = MostSoldBookInCategorySerializer(bookData)
                return Response(serializer.data)
            else:
                return Response([], status=status.HTTP_404_NOT_FOUND)  
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def searchBook(request, search_keyword):                   # Get the list of books which matches the book title or the book's author name as the search keyword
    try:
        bookData = Book.objects.filter(Q(title__icontains = search_keyword)| Q(author__name__icontains = search_keyword))
        if len(bookData) > 0:
            serializer = SearchBookSerializer(bookData, many=True)
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND)
        return Response(serializer.data)
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def getBooksByAuthorID(request, authorID):                   # Get the list of books by an Author using the Author ID
    try:
        authorData = Author.objects.filter(authorId=authorID)
        if len(authorData) > 0:
            bookData = Book.objects.filter(author=authorData[0])
            serializer = GetBookByAuthorIDSerializer(bookData, many=True)
            return Response(serializer.data)
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response(e, status=status.HTTP_400_BAD_REQUEST)