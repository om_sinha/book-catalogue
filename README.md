
# Project Title

A Book Catalogue Project which helps the user to Create Author, Book, Book Category, Get List of Categories
, Get Most Sold By an Author, Get Most Sold Book in the Category, Search a Book and Get list of Authors and Books
by an Author


## Installation

Clone the project

```bash
  git clone https://bitbucket.org/om_sinha/book-catalogue.git
```

Setup the Virtual environment 

```bash
  pip install virtualenv
  virtualenv env
```
Activate the virtualenv 
- For Windows

```bash
   env\Script\activate   
```

- For Mac/Ubuntu

```bash
   source env/bin/activate   
```




## Run Locally

Go to the project directory

```bash
  cd BookCatalogue
```

Install dependencies

```bash
  pip install -r requirements.txt
```

Run the migrations

```bash
  python manage.py makemigrations
  python manage.py migrate
```

Start the server

```bash
  python manage.py runserver
```


## API Reference

- Get list of Categories

```http
  GET /api/list_of_categories
```

| Type     | Description                |
| :------- | :------------------------- |
| `Array` | Returns an array of objects containing name of Categories  |


- Get list of Authors

```http
  GET /api/list_of_author
```

| Type     | Description                |
| :------- | :------------------------- |
| `Array` | Returns an array of objects containing name of Authors  |

- Add a Category

```http
  POST /api/add_category
```
```bash
curl -i -H 'Accept: application/json' -d '{"categoryId": "", "name": ""}' http://127.0.0.1:8000/api/add_category
```


| Type     | Description                |
| :------- | :------------------------- |
| `Object` | Returns the created Category Object |



- Add An Author

```http
  POST /api/add_author
```
```bash
curl -i -H 'Accept: application/json' -d '{
    "authorId": "",
    "name": "",
    "phone_no": "",
    "birth_date": "",
    "death_date": ""
}' http://127.0.0.1:8000/api/add_author
```


| Type     | Description                |
| :------- | :------------------------- |
| `Object` | Returns the created Author Object  |



- Get list of Books By Author ID

```http
  GET /api/book_by_author/${authorID}
```
```bash
curl -i -H 'Accept: application/json' http://127.0.0.1:8000/api/book_by_author/${authorID}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `authorID`      | `string` | **Required**. Author Id of the Author |



- Search a Book

```http
  GET /api/book/${search_keyword}
```
```bash
curl -i -H 'Accept: application/json' http://127.0.0.1:8000/api/book/${search_keyword}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `search_keyword`      | `string` | **Required**. Title of the book or Name of the Author|



- Most Book Sold By an Author

```http
  GET /api/most_books_sold_by_author/${authorID}
```
```bash
curl -i -H 'Accept: application/json' http://127.0.0.1:8000/api/most_books_sold_by_author/${authorID}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `authorID`      | `string` | **Required**. Author Id of the Author|



- Most Book Sold In a Category

```http
  GET /api/most_books_sold_by_category/${category}
```
```bash
curl -i -H 'Accept: application/json' http://127.0.0.1:8000/api/most_books_sold_by_category/${category}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `category`      | `string` | **Required**. Category of the Book|




- Add A Book

```http
  POST /api/add_book/
```
```bash
curl -i -H 'Accept: application/json' -d '{
    "bookId": "",
    "title": "",
    "publisher": "",
    "publish_date": "",
    "price": "",
    "sold_count": "",
    "author": "",
    "categoryId": ""
}' http://127.0.0.1:8000/api/add_book/
```


| Type     | Description                |
| :------- | :------------------------- |
| `Object` | Returns the created Book Object  |